#ifndef PNG_CLASS_H
#define PNG_CLASS_H

#pragma pack(push,1)

#include "stdafx.h"
using std::ostream;
using std::istream;
using std::endl;

class bmp_header
{
public:
	unsigned short int	signature;			//sygnatura bmp
	unsigned int		size;				//rozmiar pliku
	int					reserved;			//4 bajty zarezerwowane - do sprawdzania flagi - moreimages oraz liczby bajtow w ostatniej bitmapie
	unsigned int		dataoffset;			//adres startu danych obrazka

	bmp_header();							//konstruktor
	friend ostream& operator<<(ostream& wyj, bmp_header& h);
	
	
};

class bmp_info_header
{
public:
	unsigned int	hd_size;				//rozmiar tego naglowka (40 bytes)		
	int				img_width;				//szerokosc obrazka w pikselach
	int				img_height;				//wysokosc obrazka w pikselach
	unsigned short	col_planes_used;		//ilosc palet colorow = 1
	unsigned short	bits_per_pixel;			//g��bia kolor�w (np 32b/pix)
	unsigned int	compression_method;		//uzyta kompresja
	unsigned int	image_size;				//rozmiar danych obrazu (z pominieciem naglowkow)
	int				horizontal_res;			//rozdzielczosc pozioma (pix/metr)
	int				vertical_res;			//rozdzielczosc pionowa (pix/metr)
	unsigned int	color_palete;			//paleta color�w
	unsigned int	important_colors;		//ilosc waznych kolorw (0 jesli wszystkie wazne)

	bmp_info_header();						//konstruktor
	friend ostream& operator<<(ostream& wyj, bmp_info_header& h);

};


class BMP
{
public:
	bmp_header header;						//naglowek pliku
	bmp_info_header info_header;			//naglowek informacyjny
	unsigned int * pixels;					//tablica pikseli
	unsigned int pixcount;
	BMP();									//konstruktor
	~BMP();


	bool set_size(int w, int h);			//do ustawiania wymiarow obrazka w pikselach
	void Write(std::string filename);		//zapisze plik do filename
	void Read(std::string filename);		//wczyta bitmape z filename
	friend ostream& operator<<(ostream& wyj, BMP& h);

	void SetRes(int what);
	void SetRes(bool more, unsigned int rest_bytes);
	bool more();
	int rest_bytes();
	bool SetPixelData(char * data, unsigned int bytes);

	void BMP::Write(fs::path ph);
	void BMP::Read(fs::path ph);

};


class metadata
{
public:
	uintmax_t filesize;
	unsigned int width, height, rest_bytes, bmp_count;
	char * filename;

	metadata(int fnsize=0);
	metadata(BMP& bmp);

	friend ostream& operator<<(ostream& wyj, metadata& ob);

};



#pragma pack(pop)




#endif