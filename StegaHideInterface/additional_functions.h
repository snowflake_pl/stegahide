#ifndef FUNKCJE_DODATKOWE
#define FUNKCJE_DODATKOWE
#include "stdafx.h"

using std::string;
void str_reverse(char * str, int size);
void str_reverse(std::string& s);
bool Disasemble(fs::ifstream& plik,string password, fs::path filepath, fs::path ph, int w, int h, CProgressCtrl& slider);
bool Reasemble(fs::ofstream& plik,string password, fs::path ph,CProgressCtrl& slider);
void Szyfruj(unsigned int * toEncrypt, unsigned int n, std::string password);
void GenerateHtml(string path, unsigned int bmp_count);
void GenerateHtml(fs::path ph, unsigned int bmp_count);
double log2(double n);
void GenerateMetadata(fs::path filepath, fs::path ph, unsigned int w, unsigned int h);

#endif