
// StegaHideInterfaceDlg.h : header file
//

#pragma once
#include "afxcmn.h"


// CStegaHideInterfaceDlg dialog
class CStegaHideInterfaceDlg : public CDialogEx
{
// Construction
public:
	CStegaHideInterfaceDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_STEGAHIDEINTERFACE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString input_file;
	afx_msg void OnBnClickedInputFile();
	CString destination;
	afx_msg void OnBnClickedOutputFolder();
	UINT width;
	UINT Height;
	afx_msg void OnEnChangeWidth();
	afx_msg void OnEnChangeheight();
	CString password;
	afx_msg void OnEnChangeDestination();
	afx_msg void OnEnChangeInputFile();
	afx_msg void OnEnChangePassword();
	afx_msg void OnBnClickedZakoduj();
	CProgressCtrl slider_disasemble;
	CString bitmaps_folder;
	CString reasembled_file_folder;
	UINT reas_width;
	UINT reas_height;
	CProgressCtrl slider_reasemble;
	afx_msg void OnBnClickedPickBitmapsFolder();
	afx_msg void OnBnClickedPickReasembleFolder();
	CString reconst_password;
	afx_msg void OnBnClickedReasemble();
	afx_msg void OnEnChangeBitmapPath();
	afx_msg void OnEnChangeOutputPath();
	afx_msg void OnEnChangeReasPassword();
};
