
// StegaHideInterfaceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "StegaHideInterface.h"
#include "StegaHideInterfaceDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CStegaHideInterfaceDlg dialog




CStegaHideInterfaceDlg::CStegaHideInterfaceDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CStegaHideInterfaceDlg::IDD, pParent)
	, input_file(_T(""))
	, destination(_T(""))
	, width(1024)
	, Height(1024)
	, password(_T(""))
	, bitmaps_folder(_T(""))
	, reasembled_file_folder(_T(""))
	, reas_width(0)
	, reas_height(0)
	, reconst_password(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
}

void CStegaHideInterfaceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, input_file);
	DDX_Text(pDX, IDC_EDIT2, destination);
	DDX_Text(pDX, IDC_EDIT3, width);
	DDV_MinMaxUInt(pDX, width, 1, 40960);
	DDX_Text(pDX, IDC_EDIT4, Height);
	DDV_MinMaxUInt(pDX, Height, 1, 40960);
	DDX_Text(pDX, IDC_EDIT5, password);
	DDX_Control(pDX, IDC_PROGRESS1, slider_disasemble);
	DDX_Text(pDX, IDC_EDIT6, bitmaps_folder);
	DDX_Text(pDX, IDC_EDIT7, reasembled_file_folder);
	DDX_Text(pDX, IDC_EDIT8, reas_width);
	DDX_Text(pDX, IDC_EDIT9, reas_height);
	DDX_Control(pDX, IDC_PROGRESS2, slider_reasemble);
	DDX_Text(pDX, IDC_EDIT10, reconst_password);
}

BEGIN_MESSAGE_MAP(CStegaHideInterfaceDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CStegaHideInterfaceDlg::OnBnClickedInputFile)
	ON_BN_CLICKED(IDC_BUTTON2, &CStegaHideInterfaceDlg::OnBnClickedOutputFolder)
	ON_EN_CHANGE(IDC_EDIT3, &CStegaHideInterfaceDlg::OnEnChangeWidth)
	ON_EN_CHANGE(IDC_EDIT4, &CStegaHideInterfaceDlg::OnEnChangeheight)
	ON_EN_CHANGE(IDC_EDIT2, &CStegaHideInterfaceDlg::OnEnChangeDestination)
	ON_EN_CHANGE(IDC_EDIT1, &CStegaHideInterfaceDlg::OnEnChangeInputFile)
	ON_EN_CHANGE(IDC_EDIT5, &CStegaHideInterfaceDlg::OnEnChangePassword)
	ON_BN_CLICKED(IDC_BUTTON3, &CStegaHideInterfaceDlg::OnBnClickedZakoduj)
	ON_BN_CLICKED(IDC_BUTTON4, &CStegaHideInterfaceDlg::OnBnClickedPickBitmapsFolder)
	ON_BN_CLICKED(IDC_BUTTON5, &CStegaHideInterfaceDlg::OnBnClickedPickReasembleFolder)
	ON_BN_CLICKED(IDC_BUTTON6, &CStegaHideInterfaceDlg::OnBnClickedReasemble)
	ON_EN_CHANGE(IDC_EDIT6, &CStegaHideInterfaceDlg::OnEnChangeBitmapPath)
	ON_EN_CHANGE(IDC_EDIT7, &CStegaHideInterfaceDlg::OnEnChangeOutputPath)
	ON_EN_CHANGE(IDC_EDIT10, &CStegaHideInterfaceDlg::OnEnChangeReasPassword)
END_MESSAGE_MAP()


// CStegaHideInterfaceDlg message handlers

BOOL CStegaHideInterfaceDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	slider_disasemble.SetRange(0,100);
	slider_disasemble.SetPos(0);
	slider_reasemble.SetRange(0,100);
	slider_reasemble.SetPos(0);

	UpdateData(false);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CStegaHideInterfaceDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CStegaHideInterfaceDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CStegaHideInterfaceDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CStegaHideInterfaceDlg::OnBnClickedInputFile()
{
	UpdateData();
	CFileDialog input(true);
	input.DoModal();
	input_file = input.GetPathName();
	UpdateData(false);

}


void CStegaHideInterfaceDlg::OnBnClickedOutputFolder()
{
	UpdateData();
	CFolderPickerDialog input;
	input.DoModal();
	destination = input.GetPathName();
	UpdateData(false);
}


void CStegaHideInterfaceDlg::OnEnChangeWidth()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnEnChangeheight()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnEnChangeDestination()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnEnChangeInputFile()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnEnChangePassword()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnBnClickedZakoduj()
{
	UpdateData();
	if ((input_file.IsEmpty()) || (destination.IsEmpty()))
	{
		MessageBox("Podaj prawid�owe �cie�ki do pliku kodowanego i folderu wyj�ciowego");
		return;
	}

	string filepath = input_file.GetString();
	string path = destination.GetString();
	string pass = password.GetString();
	fs::path fileph(filepath);
	fs::path ph(path);

	fs::ifstream plik(fileph,std::ios::binary);
	if (!plik)
	{
		MessageBox("Nie uda�o si� otworzy� pliku do zapisu. By� mo�e jest u�ywany przez inny program");
		input_file = "B��d otwarcia pliku!";
		UpdateData(false);
		return;
	}

	if (Disasemble(plik,pass,fileph,ph,width,Height,slider_disasemble))
	{
		slider_disasemble.SetPos(0);
		MessageBox("Kodowanie zako�czone sukcesem!");
		return;
	}
	else
	{
		slider_disasemble.SetPos(0);
		MessageBox("Kodowanie zako�czone niepowodzeniem! Sprawd� ustawienia i spr�buj ponownie...");
		return;
	}

}


void CStegaHideInterfaceDlg::OnBnClickedPickBitmapsFolder()
{
	UpdateData();
	CFolderPickerDialog input;
	input.DoModal();
	bitmaps_folder = input.GetPathName();
	UpdateData(false);
}


void CStegaHideInterfaceDlg::OnBnClickedPickReasembleFolder()
{
	UpdateData();
	CFolderPickerDialog input;
	input.DoModal();
	reasembled_file_folder = input.GetPathName();
	UpdateData(false);
}


void CStegaHideInterfaceDlg::OnBnClickedReasemble()
{
	UpdateData();

	if ((reasembled_file_folder.IsEmpty()) || (bitmaps_folder.IsEmpty()))
	{
		MessageBox("Podaj prawid�owe �cie�ki folder�w wyj�ciowych i folderu z bitmapami");
		return;
	}
	string filepath = reasembled_file_folder.GetString();
	fs::path met("metadata.bmp");
	string bitmaps = bitmaps_folder.GetString();
	string pass = reconst_password.GetString();
	fs::path fileph(filepath);
	met = fileph/met;
	BMP bmp;
	bmp.Read(met);
	metadata mdata(bmp);
	string mesg ="Nazwa odtwarzanego pliku: ";
	string fn(mdata.filename);
	mesg = mesg + fn;
	MessageBox(mesg.c_str());
	reas_height = mdata.height;
	reas_width = mdata.width;
	slider_reasemble.SetRange(0,mdata.bmp_count);
	slider_reasemble.SetPos(0);

	
	fileph = fileph/fs::path(string(mdata.filename));
	reasembled_file_folder = fileph.c_str();
	fs::path ph(bitmaps);
	UpdateData(false);
	this->RedrawWindow();
	fs::ofstream plik(fileph,std::ios::binary);
	if (!plik)
	{
		MessageBox("Niepowodzenie przy otwieraniu pliku, by� mo�e istnieje i jest u�ywany przez inny program");
		input_file = "B��d otwarcia pliku!";
		UpdateData(false);
		return;
	}
	
	if (Reasemble(plik,pass,ph,slider_reasemble))
	{
		slider_reasemble.SetPos(0);
		MessageBox("Odtwarzanie zako�czone sukcesem!");
		return;
	}
	else
	{
		slider_reasemble.SetPos(0);
		MessageBox("Odtwarzanie zako�czone niepowodzeniem! Sprawd� ustawienia i spr�buj ponownie...");
		return;
	}
}


void CStegaHideInterfaceDlg::OnEnChangeBitmapPath()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnEnChangeOutputPath()
{
	UpdateData();
}


void CStegaHideInterfaceDlg::OnEnChangeReasPassword()
{
	UpdateData();
}
