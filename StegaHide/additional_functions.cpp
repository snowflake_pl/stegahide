#include "stdafx.h"
#include "additional_functions.h"


void str_reverse(char * str, int size)						//odwraca kolejnosc bajtow w stringu (lub czymkolwiek)
{															//funkcje mo�na wykorzysta� do prze��czania big/little endian
	char t;													//zmienna temp

	for (int i=0;i<size/2;i++)
	{
		t=str[i];											//zapisz i-ty znak
		str[i] = str[size-1 -i];							//nadpisz size-1-i-tym
		str[size-1 -i] = t;									//i-ty na miejse nadpisujacego go
	}
}

void str_reverse(std::string& s)							//prze�adowanie funkcji do pracy ze stringami
{
	str_reverse(const_cast<char*>(s.data()),s.length());			//wywo�aj funkcje powyzsza													
}

bool prepare_path(fs::path ph)								//funkcja przygotowuje �cie�k� w kt�rej b�d� zapisywane pliki
{
	fs::path bit("bitmaps");
	bit = ph/bit;
	
	if (!fs::exists(ph))									//directory nie istnieje
	{
		if (!fs::create_directories(ph))					//i nie uda�o si� go stworzy�
			return false;									//zwracamy niepowodzenie
	}
	else if (fs::is_directory(ph))							//ph istnieje i jest katalogiem
	{
		if (fs::exists(bit))								//istnieje cos o nazwie bitmpas w ph
		{
			if (fs::is_directory(bit))						//i jest katalogiem
				return true;								//zwroc powodzenie przygotowania
			else											//bitmaps nie jest katalogiem
				return false;								//zwroc false
		}
		else												//bitmpas w ph nie istnieje
			return fs::create_directories(bit);				//uwtorz taki katalog i zwroc status utwarzania : true przy powodzeniu
	}
	else
		return false;										//zwroc false
	
	return false;											//domsylnie niepowodzenie
}

//wywo�anie : szerokosc, wysokosc, dane do reserved w headerze bmp, czy wiecej fragmentow pliku, ile bajtow, ref na plik, sciezka dla bitmap, indeks pliku, haslo szyfrowania
void WriteChunkToBitmap(int w, int h, int reserved,bool more, unsigned int bytes, fs::ifstream& plik, fs::path bit, ULL i, string password)
{
	BMP * bmp = new BMP;									//nowy obiekt bitmapy (na wskaznikach dla pewnosci kasowania z pamieci)

	bmp->set_size(w,h);										//wymiary obrazka
	bmp->SetRes(more,reserved);								//ustaw pole reserved
	char * bufor = new char[bytes];							//bufor na tyle bajtow

	plik.read(bufor, bytes);								//wczytanie takiej liczby bajtow z pliku
	while(!bmp->SetPixelData(bufor,bytes));					//skopiowanie ich do danych pixeli
	delete [] bufor;										//skasowanie bufora danych
	bufor = 0;												//wyzerowanie wskaznika na bufor
	string current = std::to_string(i)+".bmp";				//nazwa bierzacej bitmapy
	fs::path currph(current);								//j.w. ale w zmiennej path
	fs::path filename(bit/currph);							//pelna sciezka
	Szyfruj(bmp->pixels,bmp->pixcount, password);			
	bmp->Write(filename);									//zapis bitmapy pod odpowiedni plik

	std::cout <<"Zakonczono zapisywac plik "<<filename<<endl;
	delete bmp;												//zwalnianie pamieci zajmowanej przez bmp
	bmp = 0;
}


using namespace std;
bool Disasemble(fs::ifstream& plik,string password, fs::path filepath, fs::path ph)		
{															//funkcja dzieli plik z ifstream na bitmapy zapisane w ph/bitmaps
	unsigned int file_size = fs::file_size(filepath);		//rozmiar pliku w bajtach
	fs::path bit(ph/fs::path("bitmaps"));					//�cie�ka dla bitmap

	if (!prepare_path(ph))									//sprawdzamy istnienie koniecznych katalogow
	{														//jak trzeba to je tworzymy
		cout <<"Sciezka niepoprawna - podaj inna!"<<endl;	//jak sie nie uda, to wychodzimy z funkcji z bledem
		return false;
	}

	unsigned int w, h;

	cout <<"podaj wysokosc docelowych bitmap: ";
	cin >>h;
	cout <<"podaj szerokosc docelowych bitmap: ";
	cin >>w;

	unsigned int bytes_per_bmp = 4*w*h;													//ilo�� danych na bitmape		
	unsigned int bmp_count = static_cast<unsigned int>(file_size/bytes_per_bmp);		//ilo�� wymaganych bitmap
	unsigned int rest_bytes = static_cast<unsigned int>(file_size-(bmp_count*bytes_per_bmp));

	for (ULL i=0;i<bmp_count;i++)							
		WriteChunkToBitmap(w,h,bmp_count-i,true,bytes_per_bmp,plik,bit,i,password);		//zapisywanie calych bitmap

	unsigned int rows = 1 + static_cast<int>(rest_bytes>>static_cast<int>(log2(w*4)));	//ilo�� wymaganych linii
	WriteChunkToBitmap(w,rows,rest_bytes,false,rest_bytes,plik,bit,bmp_count,password); //zapisywanie resztkowej bitmapy

	GenerateMetadata(filepath, bit, w, h);												//generuje grafike z podstawowymi danymi nt pliku
	GenerateHtml(ph,bmp_count);

	return true;											//wyjdz z funkcji
}

bool proper_directory(fs::path ph)
{	// pozniej dodad: czytanie pliku z metadanymi i sprawdzanie cyz jest odpowiednia ilosc bitmap itd
	
	if (!fs::exists(ph))			//sciezka nie isnieje
		return false;				//zwroc false
	else if (!fs::is_directory(ph))	//sciezka to nie katalog
		return false;
	else
		return true;
}


bool Reasemble(fs::ofstream& plik,string password, fs::path ph)	//sklejarka plik�w
{
	BMP * bmp;												//wska�nik na bitmape
	bool more = true;										//standardowo more = true
	ULL i=0;												//licznik nazw plikow
	
	if (!proper_directory(ph))
	{
		cout <<"podano b��dn� �cie�k� do folderu z plikami!"<<endl<<"Upewnij sie, i� wybrano folder bitmaps"<<endl;
		return false;
	}

	do
	{
		string s = std::to_string(i)+".bmp";
		fs::path bp(ph/fs::path(s));
		cout <<"przetwarzam "<<bp.filename()<<endl;			//monit na ekran	
		bmp = new BMP;										//tworzenie nowej bitmapy
		bmp->Read(bp);										//wczytanie do bitmapy danych z pliku
		Szyfruj(bmp->pixels,bmp->pixcount,password);
		more = bmp->more();									//sprawdzenie statusu more
		if (more)											//jest wiecej plikow wiec zapisz do wyjsciowego caly wektor pikseli
			plik.write(reinterpret_cast<char*>(bmp->pixels),bmp->info_header.image_size);
		else												//to ostatni plik
			plik.write(reinterpret_cast<char*>(bmp->pixels),bmp->rest_bytes());
															//wiec zapisz tylko tyle ile trzeba
		delete bmp;											//skasuj bitmape z pamieci
		bmp =0;												//wyzeruj wskaznik
		i++;												//zwieksz indeks nazwy bitmapy
	} while (more);											//powtarzaj petle dopoki sa kolejne fragmenty


	plik.close();											//zamknij plik wyjsciowy

	return true;											//zwroc powodzenie


}

void Szyfruj(unsigned int * toEncrypt, unsigned int n, string password)
{
	if (password.length()==0)								//brak hasla = brak szyfrowania
		return;

	unsigned int passlen = password.length();				//zapisz dlugosc hasla
	string bufor = password;								//kopia hasla
	for (unsigned int i=0;i<passlen;i++)
		bufor[i] = ~bufor[i];								//negacja bitowa hasla	
			
	password+=bufor;										//mamy pasword ~password
	bufor = password;										//to samo teraz w buforze
	str_reverse(bufor);										//odwracamy go i dodajemy do obecnego hasla
	password+=bufor;										//mamy "password ~password ~drowssap drowssap"
	unsigned int * pass = reinterpret_cast<unsigned int*>(const_cast<char*>(password.c_str()));
															//pobieramy adres pocz�tku c-stringu i rzutujemy na typ zgodny z szyfrowanym
															//grupuje to znaki c-stringu po cztery bajty, dlatego czterokrotnie powielamy haslo
															//zeby jego dlugosc zgadzala sie z iloscia wyrazow 4x wiekszych
		

	for (unsigned int i=0;i<n;i++)									//dla ka�dego elementu
		toEncrypt[i] = toEncrypt[i] ^ pass[i % passlen];
															//robimy bitowy XOR
}

void GenerateHtml(fs::path ph, unsigned int bmp_count)
{
	GenerateHtml(ph.string(), bmp_count);
}
void GenerateHtml(string path, unsigned int bmp_count)		//fukcja generuje plik HTML w katalogu PATH zwierajacy link do wszystkich obrazkow (do umieszczenia na stronie)
{			
	ofstream pagehtml(path+"\\site.html");					//otwarcie pliku do zapisu html
			
	if (!pagehtml)											//obsluga niepowodzenia
	{
		cout <<"HTML NIEOTWORZONY"<<path+"\\site.html"<<endl;
		return;												//monit i wyjscie z funkcji
	}

	pagehtml <<"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">"<<endl; 
	pagehtml <<"<html>"<<endl<<"     <head>"<<endl<<"      <title> Utworzono przy uzyciu StegaHide </title>  "<<endl;
	pagehtml <<"<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"<<endl<<endl<<"</head>  "<<endl<<"     <body>  "<<endl;
															//wrzucenie nag��wka html
				
	for (unsigned int j=0;j<=bmp_count;j++)
		pagehtml <<endl<<"<center> <img src=\".\\bitmaps\\"<<j<<".bmp\"> </center></br>"<<endl;
															//wrzucanie BMP_COUNT link�w do bitmap utworzonych dzielaczem
	pagehtml <<" </body>  "<<endl<<"</html>";				//stopka html
	pagehtml.close();										//zamkniecie pliku

	return;													//wyjscie z funkcji
}

double log2(double n)										//w math.h brakuje log2, wi�c trzeba go zdefiniowa�
{
	return (log(n)/log(2.0));								//z prawa o zmianie podstawy logarytmu
}

void GenerateMetadata(fs::path filepath, fs::path ph, unsigned int w, unsigned int h)
{
	string filename((filepath.filename()).string());
	unsigned int fnsize = filename.size();
	uintmax_t filesize = fs::file_size(filepath);
	unsigned int totsize = 4*sizeof(unsigned int) + fnsize + sizeof(uintmax_t);

	unsigned int bytes_per_bmp = 4*w*h;												//ilo�� danych na bitmape		
	unsigned int bmp_count = static_cast<unsigned int>(filesize/bytes_per_bmp);		//ilo�� wymaganych bitmap
	unsigned int rest_bytes = static_cast<unsigned int>(filesize-(bmp_count*bytes_per_bmp));
	char * fn = const_cast<char*>(filename.data());
	bmp_count++;
	char * bufor = new char[totsize];
	char * iterator = bufor;
	memcpy(iterator,&filesize,sizeof(uintmax_t));
	iterator+=sizeof(uintmax_t);
	memcpy(iterator,&w,sizeof(w));
	iterator+=sizeof(w);
	memcpy(iterator,&h,sizeof(h));
	iterator+=sizeof(h);
	memcpy(iterator,&rest_bytes,sizeof(rest_bytes));
	iterator+=sizeof(rest_bytes);
	memcpy(iterator,&bmp_count,sizeof(bmp_count));
	iterator+=sizeof(bmp_count);
	memcpy(iterator,fn,fnsize);
	iterator=0;

	BMP meta;
	meta.set_size(16,16);	//1kB danych

	meta.SetPixelData(bufor,totsize);
	meta.SetRes(fnsize+1);

	fs::path bmpath(ph/fs::path("metadata.bmp"));
	meta.Write(bmpath);

	delete[] bufor;
	bufor = 0;	
}