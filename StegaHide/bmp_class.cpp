#include "stdafx.h"
#include "bmp_class.h"

bmp_header::bmp_header()
	: signature('MB')									//sygnatura
	, size(0)											//rozmiar pliku razem z naglowkami
	, reserved(0)										//pole zarezerwowane
	, dataoffset(0x36)									//poczatek tablicy danych pikesli
{}


bmp_info_header::bmp_info_header()
	: hd_size(0x28)										//rozmiar naglowka to zawsze 40 bajtow (28 hex)
	, img_width(0)										//szerokosc pliku
	, img_height(0)										//wysokosc pliku
	, col_planes_used(1)								//ilosc palet kolorow
	, bits_per_pixel(32)								//ilosc bitow na pixel
	, compression_method(0)								//tryb kompresji - brak
	, image_size(0)										//rozmiar obrazka (bez headerow) w
	, horizontal_res(0)									//rozdzielczosc pozioma
	, vertical_res(0)									//rozdzielczosc pionowa
	, color_palete(0)									//ilosc kolorow w palecie
	, important_colors(0)								//wszystkie kolory sa wazne
{}

BMP::BMP()												//konstruktor klasy bitmapa
	: header()											//wywo�anie konsturktora obiektu skladowego
	, info_header()										//j.w.
	, pixels(0)											//zerowanie wskaznika
	, pixcount(0)										//zerowanie liczby pixeli
{}

bool BMP::set_size(int width, int height)				//funkcja ustawiajaca rozmiar obrazka
{
	if (width==0)										//zerowa szeroko��, niepowodzenie
		return false;		

	if (height==0)										//zerowa wysokosc, niepowodzenie
		return false;

	this->info_header.img_width=width;					//ustawiam szerokosc
	this->info_header.img_height=height;				//ustawiam wysokosc
	this->info_header.image_size=width*height*4;		//ilo�� pixeli * 4 bajty na pixel
	this->header.size = this->info_header.image_size+sizeof(bmp_info_header) + sizeof(bmp_header);
														//ustawiam rozmiar pliku w bajtach, w naglowku bmp
	this->pixcount = width*height;						//licznosc pixeli = szer*wys

	this->pixels = new  unsigned int[pixcount];			//alokacja tablicy pikseli
	
	for (unsigned int i=0;(i<pixcount);i++)
		pixels[i] = 0;									//inicjalizacja tablicy zerami
	
	return true;
}



void BMP::Write(std::string filename)					//funkcja zapisujaca bitmape do pliku o sciezce filename
{
	std::ofstream plik(filename,std::ios::binary);		//otwarcie pliku do zapisu binarnego

	if (!plik.is_open())								//plik nie jest otwarty
	{
		std::cout <<"Otwarcie pliku do zapisu sie nie powiodlo!"<<std::endl;
		return;											//monit na ekran i wyjscie z funkcji
	}

	plik.write(reinterpret_cast<char*>(&this->header),sizeof(bmp_header));
														//wpisanie do pliku naglowka bmp
	plik.write(reinterpret_cast<char*>(&this->info_header),sizeof(bmp_info_header));
														//wpisanie do pliku naglowka informacyjnego bmp
	plik.write(reinterpret_cast<char*>(this->pixels), this->info_header.image_size);
														//wpisanie do pliku tablicy pixeli
	plik.close();										//zamkniecie pliku (zapisuje zmiany w nim dokonane)
}

void BMP::Read(std::string filename)					//funkcja wczytujaca dane binarne pliku bitmapy
{
	std::ifstream file(filename,std::ios::binary);		//otwarcie pliku do binarnego odczytu

	if (!file.is_open())								//niepowodzenie
	{
		std::cout <<"Otwarcie pliku do odczytu sie nie powiodlo!"<<std::endl;
		return;											//monit i wyjscie
	}

	file.read(reinterpret_cast<char*>(&this->header),sizeof(this->header));				//wczytanie naglowka
	file.read(reinterpret_cast<char*>(&this->info_header),sizeof(this->info_header));	//wczytanie naglowka informacyjnego
	this->pixcount = this->info_header.image_size/4;									//odczyt ilosci pikseli
	this->pixels = new unsigned int[pixcount];											//alokacja odpoweidniej tablicy na piksele

	file.read(reinterpret_cast<char*>(this->pixels), this->info_header.image_size);		//wczytanie danych o pikselach do nowoutworzonej tablicy
	file.close();																		//zwolnienie pliku
}

void BMP::Write(fs::path ph)							//*******************************************//
{														//											 //
	this->Write(ph.string());							//	prze�adowania powy�szych funkcji		 //
}														//  do obs�ugi boost::filesystem::path		 //			
														//											 //	
void BMP::Read(fs::path ph)								//											 //
{														//											 //
	this->Read(ph.string());							//											 //
}														//*******************************************//

ostream& operator<<(ostream& wyj, bmp_header& h)		//wypisanie naglowka - operator
{
	wyj <<h.signature<<endl;
	wyj <<h.size<<endl;
	wyj <<h.reserved<<endl;
	wyj <<h.dataoffset<<endl;

	return wyj;
}

ostream& operator<<(ostream& wyj, bmp_info_header& h)	//jak wy�ej, tyle ze naglowek informacyjny DIB
{

	wyj <<h.hd_size<<endl;
	wyj <<h.img_width<<endl;							//szerokosc pliku
	wyj <<h.img_height<<endl;							//wysokosc pliku
	wyj <<h.col_planes_used<<endl;						//ilosc palet kolorow
	wyj <<h.bits_per_pixel<<endl;						//ilosc bitow na pixel
	wyj <<h.compression_method<<endl;					//tryb kompresji - brak
	wyj <<h.image_size<<endl;							//rozmiar obrazka (bez headerow) w
	wyj <<h.horizontal_res<<endl;						//rozdzielczosc pozioma
	wyj <<h.vertical_res<<endl;							//rozdzielczosc pionowa
	wyj <<h.color_palete<<endl;							//ilosc kolorow w palecie
	wyj <<h.important_colors<<endl;	
	
	return wyj;
}

ostream& operator<<(ostream& wyj, BMP& h)				//jak wy�ej, tyle �e cala bitmapa
{
	wyj <<h.header<<endl;
	wyj <<h.info_header<<endl;
	
	return wyj;
}

void BMP::SetRes(int what)								//ustawianie pola reserved w naglowku pliku
{
	this->header.reserved = what;
}

void BMP::SetRes(bool more, unsigned int rest_bytes)
{
	int what=rest_bytes;								//dodaj reszte bitow

	if (more)											//trzeba wiecej
		what*=-1;										//zmien znak what

	this->header.reserved = what;						//ustaw dane w naglowku
}

bool BMP::more()										//czy istnieja nastepne bitmapy?
{
	return this->header.reserved<0;						//libczba mniejsza od zera = true
}

int BMP::rest_bytes()									//rest_bytes to modul z reserved
{
	return abs(this->header.reserved);;					//zwroc go
}


BMP::~BMP()												//destruktor
{
	if (pixels)											//jesli jest tablica pixeli
		delete[] pixels;								//skasuj ja
	pixels = 0;											//zeruj wskaznik
	pixcount=0;											//zeruj liczbe pixeli
}

bool BMP::SetPixelData(char * data, unsigned int bytes)
{
	if (!data)											//nie ma czego kopiowac
		return false;

	if (!bytes)											//dlugosc do kopiowania = 0
		return false;

	if (!this->pixels)									//nie ma tablicy na pixele
		this->pixels = new unsigned int[bytes/sizeof(unsigned int)];
														//sprobuj zaalokowac

	if (!this->pixels)									//wciaz nie ma tablicy na pixele
		return false;

	memcpy(this->pixels,data,bytes);					//kopiuj dane z bufora do tablicy pixeli

	return true;										//zwroc powodzenie
}


metadata::metadata(int fnsize)
	: filesize(0)
	, width(0)
	, height(0)
	, rest_bytes(0)
	, bmp_count(0)
	, filename(0)
{
	if (fnsize)
		filename = new char[fnsize];
}

ostream& operator<<(ostream& wyj, metadata& ob)
{
	wyj <<"Rozmiar pliku: "<<ob.filesize<<" [B]"<<endl;
	wyj <<"Wymiary bitmap: "<<ob.width<<"x"<<ob.height<<" [pix]"<<endl;
	wyj <<"Ilo�� bitmap: "<<ob.bmp_count<<endl;
	wyj <<"Ilo�� bajt�w w ostatniej bitmapie: "<<ob.rest_bytes<<" [B]"<<endl;
	string s(ob.filename);
	wyj <<"Nazwa oryginalnego pliku: "<<s<<endl;

	return wyj;

}

metadata::metadata(BMP& bmp)
{
	this->metadata::metadata(bmp.header.reserved);
	memcpy(this,bmp.pixels, sizeof(metadata)-4);
	memcpy(this->filename,reinterpret_cast<char*>(bmp.pixels)+0x18,bmp.header.reserved);
}